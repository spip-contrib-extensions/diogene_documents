<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/diogene_documents?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_documents_un_par_un' => 'Exibir tantos botões navegar quanto o número máximo de documentos (se superior a 1).',

	// L
	'label_ajouter_nouveaux_documents' => 'Incluir documentos',
	'label_champs_documents' => 'Campos associados aos documentos',
	'label_documents_un_par_un' => 'Transferir os documentos um a um',
	'label_editer_documents_articles' => 'Editar os documentos vinculados',
	'label_nombre_documents' => 'Número máximo de documentos (0 para ilimitado)',
	'legende_documents' => 'Documentos',
	'lien_supprimer_document' => 'Excluir este documento'
);
