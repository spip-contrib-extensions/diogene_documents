<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_documents.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_documents_un_par_un' => 'Afficher autant de bouton parcourir que le nombre maximal de documents (si supérieur à 1).',

	// L
	'label_ajouter_nouveaux_documents' => 'Ajouter des documents',
	'label_champs_documents' => 'Champs associés aux documents',
	'label_documents_un_par_un' => 'Téléverser les documents un par un',
	'label_editer_documents_articles' => 'Éditer les documents liés',
	'label_nombre_documents' => 'Nombre de document maximal (0 pour l’infini)',
	'legende_documents' => 'Documents',
	'lien_supprimer_document' => 'Supprimer ce document'
);
