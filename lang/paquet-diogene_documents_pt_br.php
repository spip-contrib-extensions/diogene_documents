<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-diogene_documents?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_documents_description' => 'Inclui a possibilidade de vincular documentos a uma matéria a partir do seu próprio formulário.',
	'diogene_documents_nom' => 'Diogenes - Documentos',
	'diogene_documents_slogan' => 'Complemento documentos para o "Diogenes"'
);
