<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/diogene_documents.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'diogene_documents_description' => 'Ajoute la possibilité d’ajouter des documents à un article depuis son propre formulaire.',
	'diogene_documents_nom' => 'Diogene - Documents',
	'diogene_documents_slogan' => 'Complément documents pour "Diogène"'
);
